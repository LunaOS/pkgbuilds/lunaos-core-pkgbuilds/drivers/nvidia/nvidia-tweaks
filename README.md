# Nvidia Tweaks

This repository contains all the configurations that are used in LunaOS for Nvidia GPU

## This repository uses code from

- [negativo17](https://github.com/negativo17/nvidia-kmod-common)
- [RPM Fusion](https://github.com/rpmfusion/xorg-x11-drv-nvidia/)
